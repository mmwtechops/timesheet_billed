﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;


namespace TechOpsSchedule
{
    public class DBAccess
    {
        private SqlConnection _currentConnection;
        private String _connectionStr;

        public void SetConnection(String connStr)
        {
            _currentConnection = new SqlConnection(connStr);
            _connectionStr = connStr;
        }

        public String GetConnectionString()
        {
            return _connectionStr;
        }

        public SqlConnection GetCurrentConnection()
        {
            return _currentConnection;
        }

        public void OpenConnection()
        {
            try
            {                
                _currentConnection.Open();
            }
            catch (SqlException e)
            {
            }
        }

        public void CloseConnection()
        {
            _currentConnection.Close();
        }

        public void ExecuteSQLQueryCommand(String strCmd, List<String> queryStr ,ref List<String> output)
        {
            using (SqlCommand cmd = new SqlCommand(strCmd, _currentConnection))
            {
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    foreach (String s in queryStr)
                    {
                        output.Add(reader[s].ToString());
                    }
                }

                reader.Close();
            }
        }

        public void ExecuteSQLUpdateTimesheet(String strCmd, int acctMgrID, String timeSpent, int fileCount, int chargeable, 
            String comments, String jobRunID, String taskType, int programmerID, String operationCode)
        {
            using (SqlCommand cmd = new SqlCommand(strCmd, _currentConnection))
            {
                SqlTransaction transaction;
                transaction = _currentConnection.BeginTransaction("TimesheetUpdateTransaction");
                cmd.Transaction = transaction;

                if (programmerID == 0)
                {
                    cmd.Parameters.Add("@Programmer_ID", SqlDbType.Int).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Programmer_ID", SqlDbType.Int).Value = programmerID;
                }

                if (acctMgrID == 0)
                {
                    cmd.Parameters.Add("@AccountManager_ID", SqlDbType.Int).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@AccountManager_ID", SqlDbType.Int).Value = acctMgrID;
                }
                      
                cmd.Parameters.Add("@TimeSpent", SqlDbType.Decimal).Value = Convert.ToDecimal(timeSpent);

                if (operationCode == "")
                {
                    cmd.Parameters.Add("@OperationCode", SqlDbType.VarChar).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@OperationCode", SqlDbType.VarChar).Value = operationCode;
                }
                
                cmd.Parameters.Add("@FileCount", SqlDbType.Int).Value = fileCount;
                cmd.Parameters.Add("@Chargeable", SqlDbType.Int).Value = chargeable;
                cmd.Parameters.Add("@IsProcessed", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@Comments", SqlDbType.VarChar).Value = comments;
                cmd.Parameters.Add("@JobRun_ID", SqlDbType.Int).Value = int.Parse(jobRunID);
                cmd.Parameters.Add("@JobType", SqlDbType.VarChar).Value = taskType;
                cmd.Parameters.Add("@DateUpdated", SqlDbType.DateTime).Value = DateTime.Now;

                try
                {
                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    // TO DO - Display Error Message
                    try 
                    {
                        transaction.Rollback();
                    } 
                    catch (Exception ex2) 
                    {
                        // TO DO - Display Error Message
                    }
                } 
                
            } 
        }

        public void ExecuteSQLInsertTimesheet(String strCmd, String jobRunID, String jobNo, int runNo, String client, String taskType, int programmerID,
            int acctContactID, int acctMgrID, String startTime, Decimal timeSpent, String operationCode, String comments, int chargeable, String chargeableReason, int automated, int billed,
            int fileCount, int inputRecords, int outputrecords, int excludedRecords)
        {
            using (SqlCommand cmd = new SqlCommand(strCmd, _currentConnection))
            {
                SqlTransaction transaction;
                transaction = _currentConnection.BeginTransaction("TimesheetInsertTransaction");
                cmd.Transaction = transaction;

                cmd.Parameters.Add("@JobRun_ID", SqlDbType.NVarChar).Value = jobRunID;
                cmd.Parameters.Add("@JobNo", SqlDbType.VarChar).Value = jobNo;
                cmd.Parameters.Add("@RunNo", SqlDbType.Int).Value = runNo;
                cmd.Parameters.Add("@Client", SqlDbType.NVarChar).Value = client;
                cmd.Parameters.Add("@TaskType", SqlDbType.NChar).Value = taskType;
                if (programmerID == 0)
                {
                    cmd.Parameters.Add("@Programmer_ID", SqlDbType.Int).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Programmer_ID", SqlDbType.Int).Value = programmerID;
                }

                if (acctContactID == 0)
                {
                    cmd.Parameters.Add("@ClientService_ID", SqlDbType.Int).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@ClientService_ID", SqlDbType.Int).Value = acctContactID;
                }

                if (acctMgrID == 0)
                {
                    cmd.Parameters.Add("@AccountManager_ID", SqlDbType.Int).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@AccountManager_ID", SqlDbType.Int).Value = acctMgrID;
                }
                
                cmd.Parameters.Add("@StartTime", SqlDbType.DateTime).Value = startTime;
               
                cmd.Parameters.Add("@TimeSpent", SqlDbType.Decimal).Value = timeSpent;
                if (operationCode == "")
                {
                    cmd.Parameters.Add("@OperationCode", SqlDbType.VarChar, 50).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@OperationCode", SqlDbType.VarChar, 50).Value = operationCode;
                }
                
                cmd.Parameters.Add("@Comments", SqlDbType.NChar).Value = comments;
                cmd.Parameters.Add("@Chargeable", SqlDbType.TinyInt).Value = chargeable;
                if (chargeable == 1)
                {
                    cmd.Parameters.Add("@NonChargeableReason", SqlDbType.NVarChar).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@NonChargeableReason", SqlDbType.NVarChar).Value = chargeableReason;
                }

                cmd.Parameters.Add("@Automated", SqlDbType.TinyInt).Value = automated;
                cmd.Parameters.Add("@Billed", SqlDbType.TinyInt).Value = billed;
                cmd.Parameters.Add("@FileCount", SqlDbType.Int).Value = fileCount;
                cmd.Parameters.Add("@InputRecords", SqlDbType.Int).Value = inputRecords; 
                cmd.Parameters.Add("@OutputRecords", SqlDbType.Int).Value = outputrecords;
                cmd.Parameters.Add("@ExcludedRecords", SqlDbType.Int).Value = excludedRecords;

                try
                {
                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    // TO DO - Display Error Message
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        // TO DO - Display Error Message
                    }
                }
            }
        }
    }
}