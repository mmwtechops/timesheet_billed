﻿<%@ Page Title="Lookup Detail" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LookupDetails.aspx.cs" Inherits="TimesheetSystem.Admin.LookupDetails" %>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
<script type="text/javascript">
    function Validate() {

        var strUserName = document.getElementById("txtUserName");

        var strValidation = "";
        if (strUserName.value == "") {
            strValidation = strValidation + "\n  - Lookup Code.";
        }

        if (strValidation != "") {

            alert("Please enter:" + strValidation);

            return false;

        } else {

            return true;
        }
    }
</script> 
<div>
<table>
    <tr>
        <td></td>
        <td></td>
        <td><asp:Label ID="FormHeading" runat="server" Font-Size="Large" Font-Bold="true" BackColor="White">Edit Lookup</asp:Label></td>
    </tr>
    <tr>
        <td></td>
        <td><asp:Label ID="Label8" runat="server" CssClass="lbl" Text="ID" Font-Bold="true"></asp:Label></td>
        <td><asp:TextBox ID="txtID" runat="server" Font-Size="14px" ReadOnly="true" BackColor="ButtonFace"></asp:TextBox></td>
    </tr>
    <tr>
        <td></td>
        <td><asp:Label ID="Label1" runat="server" CssClass="lbl" Text="Lookup Name" Font-Bold="true"></asp:Label></td>
        <td><asp:DropDownList ID="ddlLookupListName_ID" runat="server" Font-Size="14px" ></asp:DropDownList></td>
    </tr>
    <tr>
        <td></td>
        <td><asp:Label ID="Label5" runat="server" CssClass="lbl" Text="Lookup Condition" Font-Bold="true"></asp:Label></td>
        <td><asp:TextBox ID="txtLookupListCondition" ClientIDMode="Static" MaxLength="100" runat="server" Font-Size="14px" ></asp:TextBox></td>
    </tr>
    <tr>
        <td></td>
        <td><asp:Label ID="Label2" runat="server" CssClass="lbl" Text="Integer" Font-Bold="true"></asp:Label></td>
        <td><asp:TextBox ID="txtLookupListInt" ClientIDMode="Static" MaxLength="10" runat="server" Font-Size="14px" ></asp:TextBox></td>
    </tr>
    <tr>
        <td></td>
        <td><asp:Label ID="Label17" runat="server" CssClass="lbl" Text="Code" Font-Bold="true"></asp:Label></td>
        <td><asp:TextBox ID="txtLookupListCode" MaxLength="50" runat="server" Font-Size="14px" ></asp:TextBox></td>
    </tr>
    <tr>
        <td></td>
        <td><asp:Label ID="Label21" runat="server" CssClass="lbl" Text="Description" Font-Bold="true"></asp:Label></td>
        <td><asp:TextBox ID="txtLookupListDesc" MaxLength="50" runat="server" Font-Size="14px" ></asp:TextBox></td>
    </tr>
    <tr>
        <td></td>
        <td><asp:Label ID="Label3" runat="server" CssClass="lbl" Text="Sort Order" Font-Bold="true"></asp:Label></td>
        <td><asp:TextBox ID="txtLookupListSortOrder" MaxLength="10" runat="server" Font-Size="14px" ></asp:TextBox></td>
    </tr>
    <tr>
        <td></td>
        <td><asp:Label ID="Label4" runat="server" CssClass="lbl" Text="Active" Font-Bold="true"></asp:Label></td>
        <td><asp:DropDownList ID="ddlIsActive" runat="server" Font-Size="14px" ></asp:DropDownList></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td style="text-align:center">
        <asp:Button ID="Save" runat="server" UseSubmitBehavior="true" OnClientClick="return Validate();" OnClick="Save_Click" Text="Save"></asp:Button>
        <asp:Button ID="Cancel" runat="server" CausesValidation="false" PostBackUrl="~/Admin/Admin.aspx" Text="Cancel"></asp:Button>
        </td>
    </tr>
    </table>    
    </div>
</asp:Content>


