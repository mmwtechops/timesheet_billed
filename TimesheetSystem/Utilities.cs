﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace TimesheetSystem
{
    // Date to String Conversion --//
    public static class DateConverter
    {
        public static string strDate;
        public static string strHH;
        public static string strMM;

        public static Boolean convertDateToString(DateTime dt)
        {
            strDate = "";
            strHH = "";
            strMM = "";

            strDate = dt.Day.ToString() + "/" + dt.Month.ToString() + "/" + dt.Year.ToString();
            if (dt.Hour < 10)
            {
                strHH = "0";
            }
            strHH = strHH + dt.Hour.ToString();
            if (dt.Minute < 10)
            {
                strMM = "0";
            }
            strMM = strMM + dt.Minute.ToString();

            return true;
        }
    }

    //-------------------------------------------------------------------------------//
    //-- MMWUser - Retrieve MMW User Details for a particular user                 --//
    //-------------------------------------------------------------------------------//

    public class MMWUser
    {
        public int intUserID;
        public string strUserName;
        public string strUserDisplayName;
        public string strFirstName;
        public string strLastName;
        public string strAccessRole;

        //-- Permissions --//

        public string strJobAccess;
        public string strJobRunAccess;
        public string strJobRunTaskAccess;
        public bool blnIsAdmin;

        public MMWUser()
        {
            this.intUserID = 0;
            this.strUserName = "";
            this.strUserDisplayName = "";
            this.strFirstName = "";
            this.strLastName = "";
            this.strAccessRole = "";

            this.strJobAccess = "Read";
            this.strJobRunAccess = "Read";
            this.strJobRunTaskAccess = "Read";
            this.blnIsAdmin = false;
        }

        public bool getMMWUserDetails(String strUserName)
        {
            if (strUserName.Length > 0)
            {
                this.strUserName = strUserName;
            }

            if (this.strUserName.Length > 0)
            {
                //-- Strip Domain --//

                int ia;
                for (ia = 0; ia < this.strUserName.Length && this.strUserName[ia] != '\\'; ia++) ;
                if (ia < this.strUserName.Length && this.strUserName[ia] == '\\')
                {
                    this.strUserName = this.strUserName.Substring(ia + 1, this.strUserName.Length - ia - 1);
                }

                //-- Lookup User Name --//

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

                SqlCommand cmd3 = new SqlCommand("SELECT ID, UserName, UserDisplayName, FirstName, LastName, AccessRole FROM MMWUser WITH (NOLOCK) WHERE UserName = @UserName", conn);
                cmd3.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = this.strUserName;

                conn.Open();
                SqlDataReader reader2 = cmd3.ExecuteReader();

                if (reader2.Read())
                {
                    this.intUserID = int.Parse(reader2["ID"].ToString());
                    this.strUserName = reader2["UserName"].ToString();
                    this.strUserDisplayName = reader2["UserDisplayName"].ToString();
                    this.strFirstName = reader2["FirstName"].ToString();
                    this.strLastName = reader2["LastName"].ToString();
                    this.strAccessRole = reader2["AccessRole"].ToString();
                }

                conn.Close();

                //-- Set Permissions --//

                //-- Admin --//
                //-- Create All --//

                if (this.strAccessRole == "Admin")
                {
                    this.strJobAccess = "Create";
                    this.strJobRunAccess = "Create";
                    this.strJobRunTaskAccess = "Create";
                    this.blnIsAdmin = true;
                }

                //-- IT --//
                //-- Edit Sekected --//

                if (this.strAccessRole == "IT")
                {
                    this.strJobAccess = "Read";
                    this.strJobRunAccess = "Read";
                    this.strJobRunTaskAccess = "Update";
                    this.blnIsAdmin = false;
                }

                //-- CS --//
                //-- Read All --//

                if (this.strAccessRole == "CS")
                {
                    this.strJobAccess = "Read";
                    this.strJobRunAccess = "Read";
                    this.strJobRunTaskAccess = "Read";
                    this.blnIsAdmin = false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    //-------------------------------------------------------------------------------//
    //-- Lookup - Retrieve lookup values from DB for a Lookup List Name            --//
    //-------------------------------------------------------------------------------//

    public class Lookup
    {
        public int intLookupIndex;
        public int[] intLookupID = new int[1000]; 
        public int[] intLookupInt = new int[1000];
        public String[] strLookupCode = new String[1000];
        public String[] strLookupDesc = new String[1000];
        public bool[] boolIsDefaultTrue = new bool[1000];

        public Lookup()
        {
            this.intLookupIndex = 0;
        }

        public bool getValues(String strListName, String strListCondition)
        {
            this.intLookupIndex = 0;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            {
                String strCmdStr;
                if (strListCondition.Length > 0)
                {
                    strCmdStr = "SELECT LookupListValue_ID, LookupListInt, LookupListCode, LookupListDesc, IsDefaultTrue FROM uv_LookupListValue_Select WITH (NOLOCK) WHERE LookupListName = '" + strListName + "' AND LookupListCondition = '" + strListCondition + "' ORDER BY LookupListSortOrder";
                }
                else
                {
                    strCmdStr = "SELECT LookupListValue_ID, LookupListInt, LookupListCode, LookupListDesc, IsDefaultTrue FROM uv_LookupListValue_Select WITH (NOLOCK) WHERE LookupListName = '" + strListName + "' ORDER BY LookupListSortOrder";
                }

                using (SqlCommand cmd = new SqlCommand(strCmdStr, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        this.intLookupID[this.intLookupIndex] = (int)reader["LookupListValue_ID"];
                        if (reader["LookupListInt"].ToString().Length > 0)
                        {
                            this.intLookupInt[this.intLookupIndex] = (int)reader["LookupListInt"];
                        }
                        else
                        {
                            this.intLookupInt[this.intLookupIndex] = 0;
                        }
                        this.strLookupCode[this.intLookupIndex] = reader["LookupListCode"].ToString();
                        this.strLookupDesc[this.intLookupIndex] = reader["LookupListDesc"].ToString();
                        if (reader["IsDefaultTrue"].ToString() == "1")
                        {
                            this.boolIsDefaultTrue[this.intLookupIndex] = true;
                        }
                        else
                        {
                            this.boolIsDefaultTrue[this.intLookupIndex] = false;
                        }                        
                        this.intLookupIndex++;
                    }

                    conn.Close();
                }
            }

            return true;
        }

        public bool getTaskValues(string strWorkflowName, string strWorkflowParentCode)
        {
            this.intLookupIndex = 0;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            {
                String strCmdStr;
                if (strWorkflowParentCode == "")
                {
                    strCmdStr = "SELECT WorkFlowCode, WorkflowParentCode FROM uv_WorkflowFlow WHERE WorkflowName = '" + strWorkflowName + "' AND WorkflowParentCode IS NULL ORDER BY WorkFlowCode";
                }
                else
                {
                    strCmdStr = "SELECT WorkFlowCode, WorkflowParentCode FROM uv_WorkflowFlow WHERE WorkflowName = '" + strWorkflowName + "' AND ( WorkflowParentCode = '" + strWorkflowParentCode + "' OR WorkflowCode = '" + strWorkflowParentCode + "' ORDER BY WorkFlowCode";
                }

                using (SqlCommand cmd = new SqlCommand(strCmdStr, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        this.strLookupCode[this.intLookupIndex] = reader["WorkFlowCode"].ToString();
                        this.strLookupDesc[this.intLookupIndex] = reader["WorkFlowCode"].ToString();
                        this.intLookupIndex++;
                    }

                    conn.Close();
                }
            }

            return true;
        }
    }

    //-- TO DO -- Standardise String/string --//

    public class LookupCostCentre
    {
        public int intLookupIndex;
        
        public String[] strLookupCostCentre = new String[1000];
        public String[] strLookupLineItem = new String[1000];
        public String[] strLookupCostTaskType = new String[1000];
        public String[] strLookupOperationName = new String[1000];
        public int[] jobRelatedFlag = new int[1000];
        

        public LookupCostCentre()
        {
            this.intLookupIndex = 0;
        }

        public String getCostCentreCode(String lineItem)
        {
            String opCode = "";
            for (int i = 0; i < intLookupIndex; ++i)
            {
                if (lineItem == strLookupLineItem[i] || lineItem == strLookupOperationName[i])
                {
                    opCode = strLookupCostCentre[i];
                    break;
                }
            }
            return opCode;
        }

        public String getCostCentreDesc(String operationCode)
        {
            String costCentreDesc = "";
            for (int i = 0; i < intLookupIndex; ++i)
            {
                if (operationCode == strLookupCostCentre[i])
                {
                    costCentreDesc = strLookupLineItem[i];      
                    break;
                }
            }
            return costCentreDesc;
        }

        //-- TO DO -- Put this into a class --//

        public bool getValues()
        {
            this.intLookupIndex = 0;

            this.strLookupCostTaskType[this.intLookupIndex] = "";
            this.strLookupCostCentre[this.intLookupIndex] = "";
            this.strLookupLineItem[this.intLookupIndex] = "";
            this.strLookupOperationName[this.intLookupIndex] = "";
            this.intLookupIndex++;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            {
                String strCmdStr;
                strCmdStr = "SELECT CostCentreCode, CostCentreDesc, TaskType, JobRelated, OperationName FROM Lookup_CostCentre WITH (NOLOCK) ORDER BY CostCentreDesc";
                
                using (SqlCommand cmd = new SqlCommand(strCmdStr, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        this.strLookupCostCentre[this.intLookupIndex] = reader["CostCentreCode"].ToString();
                        this.strLookupLineItem[this.intLookupIndex] = reader["CostCentreDesc"].ToString();
                        this.strLookupCostTaskType[this.intLookupIndex] = reader["TaskType"].ToString();
                        this.jobRelatedFlag[this.intLookupIndex] = Int32.Parse(reader["JobRelated"].ToString());
                        this.strLookupOperationName[this.intLookupIndex] = reader["OperationName"].ToString();

                        this.intLookupIndex++;
                    }

                    conn.Close();
                }
            }

            return true;
        }

    }

    public class LookupTaskType
    {
        public int intLookupIndex;

        public String[] strLookupTaskType = new String[1000];
        public String[] strLookupTaskDescription = new String[1000];


        public LookupTaskType()
        {
            this.intLookupIndex = 0;
        }

        public String getJobType(String jobTypeDesc)
        {
            String jobType = "";
            for (int i = 0; i < intLookupIndex; ++i)
            {
                if (jobTypeDesc == strLookupTaskDescription[i])
                {
                    jobType = strLookupTaskType[i];
                    break;
                }
            }
            return jobType;
        }

        public String getJobDesc(String jobType)
        {
            String jobDesc = "";
            for (int i = 0; i < intLookupIndex; ++i)
            {
                if (jobType == strLookupTaskType[i])
                {
                    jobDesc = strLookupTaskDescription[i];
                    break;
                }
            }
            return jobDesc;
        }

        public bool getValues()
        {
            this.intLookupIndex = 0;

            this.strLookupTaskType[this.intLookupIndex] = "";
            this.strLookupTaskDescription[this.intLookupIndex] = "";
            this.intLookupIndex++;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            {
                String strCmdStr;
                strCmdStr = "SELECT JobType, Description FROM Lookup_JobType WITH (NOLOCK) ORDER BY JobType";

                using (SqlCommand cmd = new SqlCommand(strCmdStr, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        this.strLookupTaskType[this.intLookupIndex] = reader["JobType"].ToString();
                        this.strLookupTaskDescription[this.intLookupIndex] = reader["Description"].ToString();

                        this.intLookupIndex++;
                    }

                    conn.Close();
                }
            }

            return true;
        }
    }


    public class LookupReason
    {
        public int intLookupIndex;

        public String[] strLookupReason = new String[1000];
        public String[] strLookupDescription = new String[1000];


        public LookupReason()
        {
            this.intLookupIndex = 0;
        }

        public bool getValues()
        {
            this.intLookupIndex = 0;

            this.strLookupReason[this.intLookupIndex] = "";
            this.strLookupDescription[this.intLookupIndex] = "";
            this.intLookupIndex++;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            {
                String strCmdStr;
                strCmdStr = "SELECT Reason, Description FROM Lookup_Reason WITH (NOLOCK) ";

                using (SqlCommand cmd = new SqlCommand(strCmdStr, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        this.strLookupReason[this.intLookupIndex] = reader["Reason"].ToString();
                        this.strLookupDescription[this.intLookupIndex] = reader["Description"].ToString();

                        this.intLookupIndex++;
                    }

                    conn.Close();
                }
            }

            return true;
        }

    }


    //-------------------------------------------------------------------------------//
    //-- MMWContact - Retrieve all contacts from DB                                --//
    //-------------------------------------------------------------------------------//

    public class MMWContact
    {
        public int intContactIndex;
        public int[] intContactID = new int[1000];
        public String[] strContactUserName = new String[1000];
        public String[] strContactDisplayName = new String[1000];
        public String[] strFullName = new String[1000];

        public DataTable tabContact = new DataTable();

        public MMWContact()
        {
            this.intContactIndex = 0;
        }

        public int getContactIndex(int contactID)
        {
            int retVal = 0;
            for (int i=0; i < intContactIndex; ++i)
            {
                if (intContactID[i] == contactID)
                {
                    retVal = i;
                    break;
                }
            }
            return retVal;
        }

        public bool getValues(String strAccessRole)
        {
            this.intContactIndex = 0;

            this.intContactID[this.intContactIndex] = 0;
            this.strContactUserName[this.intContactIndex] = "";
            this.strContactDisplayName[this.intContactIndex] = "";
            this.intContactIndex++;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            {
                String strCmdStr;
                if (strAccessRole.Length > 0)
                {
                    strCmdStr = "SELECT ID, UserName, UserDisplayName, FirstName, LastName FROM MMWUser WITH (NOLOCK) WHERE AccessRole IN (" + strAccessRole + ") ORDER BY UserDisplayName";
                }
                else
                {
                    strCmdStr = "SELECT ID, UserName, UserDisplayName, FirstName, LastName FROM MMWUser WITH (NOLOCK) ORDER BY UserDisplayName";
                }

                using (SqlCommand cmd = new SqlCommand(strCmdStr, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        this.intContactID[this.intContactIndex] = (int)reader["ID"];
                        this.strContactUserName[this.intContactIndex] = reader["UserName"].ToString();
                        this.strContactDisplayName[this.intContactIndex] = reader["UserDisplayName"].ToString();
                        this.strFullName[this.intContactIndex] = reader["FirstName"].ToString() + " " + reader["LastName"].ToString(); 
                        this.intContactIndex++;
                    }

                    conn.Close();
                }
            }

            //-- Load Dataset --//
            //-- TO DO - JUST USE DS NOT DOUBLE CALL TO DB --//

            //-- Load DS --//

            this.tabContact = new DataTable();
            string cmdstr = "";

            //-- If ReadOnly Show Selected Only --//

            if (strAccessRole.Length > 0)
            {
                cmdstr = "SELECT 0 ID, NULL AS UserDisplayName UNION SELECT ID, UserDisplayName FROM MMWUser WITH (NOLOCK) WHERE AccessRole IN (" + strAccessRole + ") ORDER BY UserDisplayName";
            }
            else
            {
                cmdstr = "SELECT 0 ID, NULL AS UserDisplayName UNION SELECT ID, UserDisplayName FROM MMWUser WITH (NOLOCK) ORDER BY UserDisplayName";
            }


            SqlCommand ADDCmd = new SqlCommand(cmdstr, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(ADDCmd);

            adapter.Fill(this.tabContact);

            return true;
        }
    }

    //-------------------------------------------------------------------------------//
    //-- MMWWorkflow - Retrieve all contacts from DB                                --//
    //-------------------------------------------------------------------------------//

    public class MMWWorkflow
    {
        public int intCurrentStatusID = 0;
        public string strCurrentStatusCode = "";

        public int intBackStatusID = 0;
        public string strBackStatusCode = "";

        public int intForwardStatusIndex = 0;
        public int[] intForwardStatusID = new int[1000];
        public string[] strForwardStatusCode = new string[1000];


        public MMWWorkflow()
        {
            this.intCurrentStatusID = 0;
            this.strCurrentStatusCode = "";
        }

        public bool getWorkflow(int currentStatusID, string taskType)//, string[] taskFlagCode)
        {
            this.intCurrentStatusID = currentStatusID;
            this.strCurrentStatusCode = "";

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            string strCommand = "";

            //-- Get Current Status Code --//

            if (this.intCurrentStatusID > 0)
            {
                strCommand = "SELECT Workflow_ID, WorkflowCode FROM uv_Workflow WHERE TaskType = '" + taskType + "' AND ID = " + currentStatusID;

                using (SqlCommand cmd = new SqlCommand(strCommand, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        this.strCurrentStatusCode = reader["WorkflowCode"].ToString();
                    }

                    reader.Close();
                    conn.Close();
                }
            }

            //-- Status Not Found, Reset So It Defaults --//

            if (this.strCurrentStatusCode == "")
            {
                this.intCurrentStatusID = 0;
            }

            //-- If No Status, Get Default Status --//

            if (this.intCurrentStatusID == 0)
            {
                strCommand = "SELECT ID, WorkflowCode FROM uv_Workflow WHERE TaskType = '" + taskType + "' AND Parent_ID IS NULL";

                using (SqlCommand cmd = new SqlCommand(strCommand, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        this.intCurrentStatusID = (int)reader["ID"];
                        this.strCurrentStatusCode = reader["WorkflowCode"].ToString();

                        //--//
                    }

                    reader.Close();
                    conn.Close();
                }
            }

            //-- Return If No Default Found --//

            if (this.intCurrentStatusID == 0)
            {
                return false;
            }

            //-- Get Backward Status --//

            strCommand = "";
            strCommand = "SELECT Parent_ID, WorkflowParentCode FROM uv_Workflow WHERE TaskType = '" + taskType + "' AND Parent_ID IS NOT NULL AND ID = " + this.intCurrentStatusID.ToString();

            using (SqlCommand cmd = new SqlCommand(strCommand, conn))
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    this.intBackStatusID = (int)reader["Parent_ID"];
                    this.strBackStatusCode = reader["WorkflowParentCode"].ToString();

                    //--//
                }

                reader.Close();
                conn.Close();
            }

            //-- Get Forward Status --//

            strCommand = "";
            strCommand = "SELECT ID, WorkflowCode, TaskFlag_ID, TaskFlagCode FROM uv_Workflow WHERE TaskType = '" + taskType + "' AND Parent_ID = " + this.intCurrentStatusID.ToString();

            this.intForwardStatusIndex = 0;

            using (SqlCommand cmd = new SqlCommand(strCommand, conn))
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    //-- Check For Branch Flags --//

                    //-- TO DO --//

                    this.intForwardStatusID[this.intForwardStatusIndex] = (int)reader["ID"];
                    this.strForwardStatusCode[this.intForwardStatusIndex] = reader["WorkflowCode"].ToString();
                    this.intForwardStatusIndex++;

                    //--//
                }

                reader.Close();
                conn.Close();
            }

            return true;
        }

        public bool setWorkflow(int currentStatusID)
        {
            this.intCurrentStatusID = currentStatusID;
            this.strCurrentStatusCode = "";

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            string strCommand = "";

            //-- Reset All Values --//

            this.intBackStatusID = 0;
            this.strBackStatusCode = "";
            this.intForwardStatusIndex = 0;

            //-- Get Current Status Code --//

            if (this.intCurrentStatusID > 0)
            {
                strCommand = "SELECT ID, WorkflowCode FROM uv_Workflow WHERE ID = " + currentStatusID;

                using (SqlCommand cmd = new SqlCommand(strCommand, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        this.strCurrentStatusCode = reader["WorkflowCode"].ToString();
                    }

                    reader.Close();
                    conn.Close();
                }
            }

            return true;
        }
    }
}