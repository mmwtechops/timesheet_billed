﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ITSchedule
{
    public partial class Admin : System.Web.UI.Page
    {
        int intLookupListNameIndex;
        int[] intLookupListNameID = new int[1000];
        String[] strLookupListNameValue = new String[1000];

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    Tab1.CssClass = "Clicked";
                    MainView.ActiveViewIndex = 0;
                }
                else
                {
                    Response.Redirect("/Account/Login.aspx");
                }

                LoadValues();
                BindGrid();
            }
            else
            {
                setValues();
            }
        }
        protected void Tab1_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Clicked";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Initial";
            MainView.ActiveViewIndex = 0;
        }

        protected void Tab2_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Clicked";
            Tab3.CssClass = "Initial";
            MainView.ActiveViewIndex = 1;
        }

        protected void Tab3_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Clicked";
            MainView.ActiveViewIndex = 2;
        }

        private void LoadValues()
        {
            //-- Populate List Name Lookup --//

            this.intLookupListNameIndex = 0;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            using (SqlCommand cmd = new SqlCommand("SELECT LookupListName_ID, LookupListName FROM uv_LookupListValue_Select GROUP BY LookupListName_ID, LookupListName ORDER BY LookupListName", conn))
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ia = 0;

                while (reader.Read())
                {
                    this.intLookupListNameID[this.intLookupListNameIndex] = (int)reader["LookupListName_ID"];
                    this.strLookupListNameValue[this.intLookupListNameIndex] = reader["LookupListName"].ToString();
                    this.intLookupListNameIndex++;

                    this.ddlListLookupName.Items.Insert(ia++, reader["LookupListName"].ToString());
                }

                conn.Close();
            }

            if (this.ddlListLookupName.Items.Count > 0)
            {
                this.ddlListLookupName.SelectedIndex = 0;
            }

            //-- Populate Leave User Name --//

            this.ddlLeaveUserName.Items.Clear();

            using (SqlCommand cmd2 = new SqlCommand("SELECT UserDisplayName FROM uv_MMWUser WHERE AccessRole IN ('IT', 'Admin') AND UserDisplayName NOT IN ('Adam', 'Amanda', 'Aron') ORDER BY UserDisplayName", conn))
            {
                conn.Open();
                SqlDataReader reader = cmd2.ExecuteReader();
                int ia = 0;

                while (reader.Read())
                {
                    this.ddlLeaveUserName.Items.Insert(ia++, reader["UserDisplayName"].ToString());
                }

                conn.Close();
            }

            if (this.ddlLeaveUserName.Items.Count > 0)
            {
                this.ddlLeaveUserName.SelectedIndex = 0;
            }
        }

        private void setValues()
        {
            //-- Populate List Name Lookup --//

            this.intLookupListNameIndex = 0;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            using (SqlCommand cmd = new SqlCommand("SELECT LookupListName_ID, LookupListName FROM uv_LookupListValue_Select GROUP BY LookupListName_ID, LookupListName ORDER BY LookupListName", conn))
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    this.intLookupListNameID[this.intLookupListNameIndex] = (int)reader["LookupListName_ID"];
                    this.strLookupListNameValue[this.intLookupListNameIndex] = reader["LookupListName"].ToString();
                    this.intLookupListNameIndex++;
                }

                conn.Close();
            }

            //-- Fix Postback URL --//

            String strLookupListName_ID = "0";
            strLookupListName_ID = this.intLookupListNameID[this.ddlListLookupName.SelectedIndex].ToString();

            this.btnNewLookup.PostBackUrl = "frmLookupDetails.aspx?lookuplistvalueid=0&lookuplistnameid=" + strLookupListName_ID;
        }

        private void BindGrid()
        {
            //-- Populate User Grid --//

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            DataSet ds = new DataSet();
            conn.Open();

            string cmdstr = "";
            cmdstr = "SELECT ID, UserName, UserDisplayName, FirstName, LastName, AccessRole, CASE WHEN IsActive = 1 THEN 'True' ELSE 'False' END AS IsActive FROM MMWUser WITH (NOLOCK) ORDER BY ID";

            SqlDataAdapter adp = new SqlDataAdapter(cmdstr, conn);
            adp.Fill(ds);
            conn.Close();
            this.gvUser.DataSource = ds;
            this.gvUser.DataBind();

            //-- Populate List Lookup Grid --//

            DataSet ds2 = new DataSet();
            conn.Open();

            cmdstr = "SELECT LookupListValue_ID AS ID, LookupListName_ID, LookupListCondition, LookupListInt, LookupListCode, LookupListDesc, LookupListSortOrder, CASE WHEN IsActive = 1 THEN 'True' ELSE 'False' END AS IsActive FROM uv_LookupListValue_Select_All WHERE LookupListName = '" + this.ddlListLookupName.SelectedItem + "' ORDER BY ID";

            SqlDataAdapter adp2 = new SqlDataAdapter(cmdstr, conn);
            adp2.Fill(ds2);
            conn.Close();
            this.gvLookup.DataSource = ds2;
            this.gvLookup.DataBind();

            //-- Populate Leave Grid --//

            String strUserDisplayName = this.ddlLeaveUserName.SelectedItem.ToString();

            DataSet ds3 = new DataSet();
            conn.Open();

            cmdstr = "SELECT ID, UserDisplayName, LeaveType, StartDate, EndDate FROM uv_MMWLeaveDetail WHERE '" + strUserDisplayName + "' = '(All)' OR UserDisplayName = '" + strUserDisplayName + "' ORDER BY UserDisplayName, StartDate";

            SqlDataAdapter adp3 = new SqlDataAdapter(cmdstr, conn);
            adp3.Fill(ds3);
            conn.Close();
            this.gvLeave.DataSource = ds3;
            this.gvLeave.DataBind();

        }
        protected void gvJob_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
        }

        protected void gvLeave_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
        }

        protected void ddlListLookupName_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void ddlLeaveUserName_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}