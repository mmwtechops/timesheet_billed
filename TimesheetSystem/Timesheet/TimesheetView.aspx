﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TimesheetView.aspx.cs" Inherits="TimesheetSystem.TimesheetView" EnableEventValidation="false"%>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript">
        function divexpandcollapse(divname, divtrname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var divtr = document.getElementById(divtrname);
            if (div.style.display == "none") {
                div.style.display = "inline";
                //divtr.style.display = "inline";
                img.src = "../Images/minus.gif";
            } else {
                div.style.display = "none";
                // divtr.style.display = "none";
                img.src = "../Images/plus.gif";
            }
        }
</script>
    <script src="../Scripts/jquery-1.9.1.js"></script>
<style type="text/css">    
        .Background
        {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }
        .Popup
        {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 400px;
            height: 350px;
        }
        .lbl
        {
            font-size:16px;
            font-style:italic;
            font-weight:bold;
        }
        .divider{
            width:150px;
            height:auto;
            display:inline-block;
        }
        .hiddencol {
            display: none;
        }
        .mytable {
            position:relative;
            width:100%;         
        }
        .gridCellHighlight {
            font-weight:bold;
            font-size:16px;
            text-align:right;
        }
    </style>
    <link href="../Content/bootstrap.css" rel="stylesheet" /> 
<table>
    <tr >
        <td style="text-align: right">
            <asp:Label ID="Label27" runat="server" CssClass="lbl" Text="Job Number" Font-Bold="true" ></asp:Label>
        </td>
        <td style="text-align:right">
            <asp:TextBox ID="txtJobNumber" MaxLength="5" runat="server" CssClass="form-control input-sm" Font-Size="14px" OnTextChanged="txtJobNumber_TextChanged" AutoPostBack="true"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="Label2" runat="server" CssClass="lbl" Text="Run Number" Font-Bold="true" ></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtRunNumber" MaxLength="5" runat="server" CssClass="form-control input-sm" Font-Size="14px" OnTextChanged="txtRunNumber_TextChanged" AutoPostBack="true"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="Label3" runat="server" CssClass="lbl" Text="Client" Font-Bold="true" ></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtClient" runat="server" CssClass="form-control input-sm" Font-Size="14px" OnTextChanged="txtClient_TextChanged" AutoPostBack="true"></asp:TextBox>   
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label4" runat="server" CssClass="lbl" Text="From" Font-Bold="true" ></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtStartDate" type="date" runat="server" AutoPostBack="false" CssClass="form-control" Font-Size="14px" OnTextChanged="txtStartDate_TextChanged"></asp:TextBox> 
        </td>   
        <td>
            <asp:Label ID="Label5" runat="server" CssClass="lbl" Text="To" Font-Bold="true" ></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtEndDate" type="date" MaxLength="10" runat="server" AutoPostBack="false" CssClass="form-control input-sm" Font-Size="14px" OnTextChanged="txtEndDate_TextChanged"></asp:TextBox>
        </td>
        <td>
            <asp:Button ID="btnSetDate" Text="Set Date" runat="server" width="65px" CssClass="btn btn-lg btn-default" OnClick="SetDate_Click" UseSubmitBehavior="true"  />
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><asp:Label ID="Label6" runat="server" CssClass="lbl" Text="Billed" Font-Bold="true"></asp:Label></td>
        <td><asp:DropDownList ID="ddlBilled" runat="server" AutoPostBack="true" Font-Size="14px" OnSelectedIndexChanged="ddlBilled_SelectedIndexChanged"></asp:DropDownList></td>
        <td><asp:Label ID="Label1" runat="server" CssClass="lbl" Text="Source" Font-Bold="true"></asp:Label></td>
        <td><asp:DropDownList ID="ddlSource" runat="server" AutoPostBack="true" Font-Size="14px" OnSelectedIndexChanged="ddlSource_SelectedIndexChanged"></asp:DropDownList></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td></td>
        <td></td>
    </tr>
</table>
    <table>
        <tr>
            <td>
                <asp:Button ID="btnSelectAll" Text="Select ALL" runat="server" width="100px" CssClass="btn btn-lg btn-default" OnClick="SelectAll_Click" UseSubmitBehavior="true"  />
            </td>
           <td >
                 <asp:button ID="btnUnSelectAll" Text="Unselect ALL" runat="server" width="100px" CssClass="btn btn-lg btn-default" OnClick="UnSelectAll_Click" UseSubmitBehavior="true" />
            </td>
            <td>
                <asp:button ID="btnClear" Text="Clear" runat="server" width="100px" CssClass="btn btn-lg btn-default" OnClick="Clear_Click" UseSubmitBehavior="true" />
                <div class="divider" />          
                <asp:button ID="btnSave" Text="Save" runat="server" width="150px" CssClass="btn btn-lg btn-default" OnClick="Save_Click" UseSubmitBehavior="true" />            
            </td>
            <td>&nbsp;</td>
            <td>   
                <asp:button ID="btnExportExcel" Text="Export To Excel" runat="server" width="150px" CssClass="btn btn-lg btn-default" OnClick="ExportExcel_Click" UseSubmitBehavior="true"  />
            </td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
        </tr>
    </table>
<table>
 <asp:GridView ID="gvTimesheet" runat="server" ShowFooter="true" AutoGenerateColumns="false" Font-Size="Small" CssClass="table-bordered" OnRowDataBound="gvTimesheet_OnRowDataBound">
    <Columns> 
        <asp:BoundField Visible="false" ItemStyle-BorderWidth="1" ItemStyle-Wrap="false" DataField="ID" HeaderText="ID"/>
        <asp:BoundField ItemStyle-BorderWidth="1" ItemStyle-Wrap="false" DataField="JobNo" HeaderText="Job Number"/>                   
        <asp:BoundField ItemStyle-BorderWidth="1" ItemStyle-Wrap="false" DataField="RunNo" HeaderText="Run Number"/>  
        <asp:BoundField ItemStyle-BorderWidth="1" ItemStyle-Wrap="false" DataField="StartTime" HeaderText="Start Time" DataFormatString="{0:yyyy-MM-dd}"/>                 
        <asp:BoundField ItemStyle-BorderWidth="1" ItemStyle-Wrap="false" DataField="SourceCode" HeaderText="Source"/>                     
        <asp:BoundField ItemStyle-BorderWidth="1" ItemStyle-Wrap="true" DataField="Client" HeaderText="Client"/>                   
        <asp:BoundField ItemStyle-BorderWidth="1" ItemStyle-Wrap="false" DataField="CampaignID" HeaderText="Campaign ID"/>                   
        <asp:BoundField ItemStyle-BorderWidth="1" ItemStyle-Wrap="true" DataField="LineItem" HeaderText="Line Item"/>                 
        <asp:BoundField ItemStyle-BorderWidth="1" ItemStyle-Wrap="false" DataField="Chargeable" HeaderText="Chargeable"/>                
        <asp:BoundField ItemStyle-BorderWidth="1" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Right" DataField="TimeSpent" HeaderText="Time Spent" /> 
        <asp:BoundField ItemStyle-BorderWidth="1" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Right" DataField="DateInserted" HeaderText="Date Inserted" DataFormatString="{0:yyyy-MM-dd}"/> 
        <asp:BoundField ItemStyle-BorderWidth="1" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Right" DataField="DateUpdated" HeaderText="Date Updated" DataFormatString="{0:yyyy-MM-dd}"/>                
        <%-- <asp:BoundField ItemStyle-BorderWidth="1" ItemStyle-Wrap="false" DataField="RoundedTimeSpent" HeaderText="Rounded Time Spent"/> --%>                 
        <asp:BoundField ItemStyle-BorderWidth="1" ItemStyle-Wrap="false" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" DataField="Billed" HeaderText="Billed" />
        <asp:TemplateField Visible="false">
            <ItemTemplate>
                <asp:HiddenField ID="HiddenBilled" Value='<%# Eval("Billed").ToString() %>' runat="server" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:templatefield ItemStyle-BorderWidth="1" HeaderText="Billed?">
            <itemtemplate> 
                <asp:checkbox ID="cbBilledSelect" CssClass="gridCB" runat="server" Checked='<%# Convert.ToBoolean(Eval("Billed").ToString().Equals("Yes") ? true : false) %>' AutoPostBack="false" OnCheckedChanged="cbBilledSelect_CheckedChanged" />
            </itemtemplate>
            <ItemStyle HorizontalAlign="Center" />
        </asp:templatefield>
        <asp:BoundField ItemStyle-BorderWidth="1" ItemStyle-Wrap="false" DataField="BilledOn" HeaderText="Billed On" DataFormatString="{0:yyyy-MM-dd}"/>     
    </Columns>
    <HeaderStyle BackColor="#0063A6" ForeColor="White" Font-Size="Small" />
</asp:GridView> 
</table>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    </asp:Content>

