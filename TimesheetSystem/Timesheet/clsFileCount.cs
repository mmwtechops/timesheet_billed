﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace TimesheetSystem
{
    public class clsFileCount
    {
        public int ID;
        public string JobNo;
        public int RunNo;
        public string Client;
        public string Section;
        public string PrinterType;
        public string FileName;
        public int DPCount;
        public DateTime MailDate;
        public DateTime LodgementDate;
        public string FileType;
        public DateTime DateInserted;
        public DateTime DateUpdated;
        public byte Billed;
        public DateTime BilledOn;

        public string SaveError;

        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

        // Date to String Conversion --//

        public string strDate;
        public string strHH;
        public string strMM;

        public clsFileCount()
        {
            //-- Initalise Defaults --//

            ID=0;
            JobNo="";
            RunNo=0;
            Client="";
            Section="";
            PrinterType="";
            FileName="";
            DPCount=0;
            MailDate=DateTime.MinValue;
            LodgementDate=DateTime.MinValue;
            FileType="";
            DateInserted=DateTime.MinValue;
            DateUpdated=DateTime.MinValue;
            Billed=0;
            BilledOn=DateTime.MinValue;

            this.SaveError = "";
        }


        public bool saveFileCount(int id)
        {
            this.SaveError = "";

            string strCmd = "";
            if (this.Billed == 1)
            {
                strCmd += "UPDATE MMWJobRunFileCounts SET Billed=@Billed, BilledOn=GETDATE()";
            }
            else
            {
                strCmd += "UPDATE MMWJobRunFileCounts SET Billed=@Billed ";
            }

            strCmd += " WHERE ID=@ID ";

            SqlCommand cmd = new SqlCommand(strCmd, conn);
            cmd.Parameters.Add("@Billed", SqlDbType.TinyInt).Value = this.Billed;
            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
            
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return true;
        }
    }
}