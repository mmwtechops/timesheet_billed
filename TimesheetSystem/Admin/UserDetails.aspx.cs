﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TimesheetSystem.Admin
{
    public partial class UserDetails : System.Web.UI.Page
    {
        int intIsActiveIndex;
        int[] intIsActiveID = new int[1000];
        String[] strIsActiveValue = new String[1000];

        int intAccessRoleIndex;
        String[] strAccessRoleValue = new String[1000];

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                setPermission();
                getLookup();
                setValues();
            }
        }

        public void setPermission()
        {
            //-- Set Permissions --//

            MMWUser mmwUser = new MMWUser();
            mmwUser.getMMWUserDetails(HttpContext.Current.User.Identity.Name);

            if (mmwUser.strAccessRole != "Admin")
            {
                this.txtUserName.Enabled = false;
                this.txtUserName.BackColor = System.Drawing.Color.LightGray;
                this.txtUserDisplayName.Enabled = false;
                this.txtUserDisplayName.BackColor = System.Drawing.Color.LightGray;
                this.txtFirstName.Enabled = false;
                this.txtFirstName.BackColor = System.Drawing.Color.LightGray;
                this.txtLastName.Enabled = false;
                this.txtLastName.BackColor = System.Drawing.Color.LightGray;
                this.ddlAccessRole.Enabled = false;
                this.ddlAccessRole.BackColor = System.Drawing.Color.LightGray;
                this.ddlIsActive.Enabled = false;
                this.ddlIsActive.BackColor = System.Drawing.Color.LightGray;

                this.Save.Visible = false;
            }
        }

        public void getLookup()
        {
            //-- Access Role --//

            //-- Is Active --//

            this.intIsActiveIndex = 0;
            this.intIsActiveID[this.intIsActiveIndex] = 0;
            this.strIsActiveValue[this.intIsActiveIndex] = "False";
            this.intIsActiveIndex++;
            this.intIsActiveID[this.intIsActiveIndex] = 1;
            this.strIsActiveValue[this.intIsActiveIndex] = "True";
            this.intIsActiveIndex++;

            //-- Access Role --//

            this.intAccessRoleIndex = 0;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            {
                using (SqlCommand cmd = new SqlCommand("SELECT LookupListCode FROM uv_LookupListValue_Select WHERE LookupListName = 'Access Role' ORDER BY LookupListSortOrder", conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        this.strAccessRoleValue[this.intAccessRoleIndex] = reader["LookupListCode"].ToString();
                        this.intAccessRoleIndex++;
                    }

                    conn.Close();
                }
            }
        }

        public void setValues()
        {
            //-- User MMW User ID --//

            String strUserID = Request.QueryString["userid"].ToString();
            if (strUserID == "")
            {
                strUserID = "0";
            }

            //-- Edit User --//

            if (strUserID != "0")
            {
                this.FormHeading.Text = "Edit User";

                //-- Load User Data --//

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                {
                    String strCommand = "";
                    strCommand = "SELECT ID, UserName, UserDisplayName, FirstName, LastName, AccessRole, IsActive FROM MMWUser WITH (NOLOCK) WHERE ID = " + strUserID;

                    using (SqlCommand cmd = new SqlCommand(strCommand, conn))
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            this.txtID.Text = strUserID;
                            this.txtUserName.Text = reader["UserName"].ToString();
                            this.txtUserDisplayName.Text = reader["UserDisplayName"].ToString();
                            this.txtFirstName.Text = reader["FirstName"].ToString();
                            this.txtLastName.Text = reader["Lastname"].ToString();

                            for (int a = 0; a < this.intAccessRoleIndex; a++)
                            {
                                this.ddlAccessRole.Items.Insert(a, this.strAccessRoleValue[a]);
                                if (this.strAccessRoleValue[a] == reader["AccessRole"].ToString())
                                {
                                    this.ddlAccessRole.SelectedIndex = a;
                                }
                            }
                            if (this.ddlAccessRole.SelectedIndex < 0 && this.intAccessRoleIndex > 0)
                            {
                                this.ddlAccessRole.SelectedIndex = 0;
                            }

                            for (int a = 0; a < this.intIsActiveIndex; a++)
                            {
                                this.ddlIsActive.Items.Insert(a, this.strIsActiveValue[a]);
                                if (this.intIsActiveID[a] == int.Parse(reader["IsActive"].ToString()))
                                {
                                    this.ddlIsActive.SelectedIndex = a;
                                }
                            }
                            if (this.ddlIsActive.SelectedIndex < 0 && this.intIsActiveIndex > 0)
                            {
                                this.ddlIsActive.SelectedIndex = 1;
                            }

                            //--//
                        }

                        reader.Close();
                        conn.Close();
                    }
                }
            }
            else
            {
                this.FormHeading.Text = "New User";

                this.txtID.Text = strUserID;
                this.txtUserName.Text = "";
                this.txtUserDisplayName.Text = "";
                this.txtFirstName.Text = "";
                this.txtLastName.Text = "";

                for (int a = 0; a < this.intAccessRoleIndex; a++)
                {
                    this.ddlAccessRole.Items.Insert(a, this.strAccessRoleValue[a]);
                }
                if (this.ddlAccessRole.SelectedIndex < 0 && this.intAccessRoleIndex > 0)
                {
                    this.ddlAccessRole.SelectedIndex = 0;
                }

                for (int a = 0; a < this.intIsActiveIndex; a++)
                {
                    this.ddlIsActive.Items.Insert(a, this.strIsActiveValue[a]);
                }
                if (this.intIsActiveIndex > 0)
                {
                    this.ddlIsActive.SelectedIndex = 1;
                }
            }
        }

        public void saveDetails()
        {
            //-- User MMW User ID --//

            String strUserID = Request.QueryString["userid"].ToString();
            if (strUserID == "")
            {
                strUserID = "0";
            }

            string strUserName = this.txtUserName.Text;
            string strUserDisplayName = this.txtUserDisplayName.Text;
            string strFirstName = this.txtFirstName.Text;
            string strLastName = this.txtLastName.Text;
            string strAccessRole = this.ddlAccessRole.Text;

            int intIsActive = 0;
            if (this.ddlIsActive.SelectedIndex == 1)
            {
                intIsActive = 1;
            }
            if (strUserID != "0")
            {
                int intUserID = int.Parse(strUserID);

                //-- Update User --//

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

                SqlCommand cmd = new SqlCommand("UPDATE MMWUser SET UserName = @UserName, UserDisplayName = @UserDisplayName, FirstName = @FirstName, LastName = @LastName, AccessRole = @AccessRole, IsActive = @IsActive WHERE ID = @ID", conn);
                cmd.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = strUserName;
                cmd.Parameters.Add("@UserDisplayName", SqlDbType.NVarChar).Value = strUserDisplayName;
                cmd.Parameters.Add("@FirstName", SqlDbType.NChar).Value = strFirstName;
                cmd.Parameters.Add("@LastName", SqlDbType.NChar).Value = strLastName;
                cmd.Parameters.Add("@AccessRole", SqlDbType.NChar).Value = strAccessRole;
                cmd.Parameters.Add("@IsActive", SqlDbType.Int).Value = intIsActive;
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = intUserID;

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            else
            {
                //-- Insert New User --//

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

                SqlCommand cmd = new SqlCommand("INSERT INTO MMWUser (UserName, UserDisplayName, FirstName, LastName, AccessRole, IsActive) SELECT @UserName, @UserDisplayName, @FirstName, @LastName, @AccessRole, @IsActive", conn);
                cmd.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = strUserName;
                cmd.Parameters.Add("@UserDisplayName", SqlDbType.NVarChar).Value = strUserDisplayName;
                cmd.Parameters.Add("@FirstName", SqlDbType.NChar).Value = strFirstName;
                cmd.Parameters.Add("@LastName", SqlDbType.NChar).Value = strLastName;
                cmd.Parameters.Add("@AccessRole", SqlDbType.NChar).Value = strAccessRole;
                cmd.Parameters.Add("@IsActive", SqlDbType.Int).Value = intIsActive;

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            saveDetails();
            Response.Redirect("~/Admin/Admin.aspx");
        }
    }
}