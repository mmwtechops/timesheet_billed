﻿<%@ Page Title="Admin" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="TimesheetSystem.Admin.Admin" %>
<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <style type="text/css">
.Initial
{
  display: block;
  padding: 4px 18px 4px 18px;
  float: left;
  background: url("../Images/InitialImage.png") no-repeat right top;
  color: Black;
  font-weight: bold;
}
.Initial:hover
{
  color: White;
  background: url("../Images/SelectedButton.png") no-repeat right top;
}
.Clicked
{
  float: left;
  display: block;
  background: url("../Images/SelectedButton.png") no-repeat right top;
  padding: 4px 18px 4px 18px;
  color: Black;
  font-weight: bold;
  color: White;
}
</style>
<div style="text-align:center">
<table style="margin:initial">
      <tr>
        <td style="width:200px"></td> 
        <td>
          <asp:Button Text="Users" BorderStyle="None" ID="Tab1" CssClass="Initial" runat="server"
              OnClick="Tab1_Click" />
          <asp:Button Text="Lookup Values" BorderStyle="None" ID="Tab2" CssClass="Initial" runat="server"
              OnClick="Tab2_Click" />
          <asp:Button Text="Leave Calendar" BorderStyle="None" ID="Tab3" CssClass="Initial" runat="server"
              OnClick="Tab3_Click" />
          <asp:MultiView ID="MainView" runat="server">
            <asp:View ID="View1" runat="server">
                <table>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                        </td>
                        <td>
                            &nbsp;
                        <td>
                    </tr>
                    <tr>
                        <td>
                            <asp:button ID="btnAddNewUser" Text="Add New User" runat="server" PostBackUrl="~/Admin/UserDetails.aspx?userid=0" />
                        </td>
                        <td>
                        </td>
                        <td>
                                <asp:button ID="Refresh" Text="Refresh" runat="server" UseSubmitBehavior="true" />
                        <td>
                    </tr>
                </table>
                <asp:GridView ID="gvUser" runat="server" AutoGenerateColumns="false" ShowFooter="false" OnRowDataBound="gvJob_OnRowDataBound">
                    <Columns> 
                        <asp:TemplateField ItemStyle-BorderWidth="1" HeaderText="ID">
                        <ItemTemplate>
                            <a href="UserDetails.aspx?userid=<%#Eval("id")%>"><%# Eval("id") %></a>
                        </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:BoundField Visible="false" ItemStyle-BorderWidth="1" DataField="ID" HeaderText="ID" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="UserName" HeaderText="User Name" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="UserDisplayName" HeaderText="Display Name" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="FirstName" HeaderText="First Name" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="LastName" HeaderText="Last Name" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="AccessRole" HeaderText="Access Role" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="IsActive" HeaderText="Is Active" />
                        <asp:TemplateField ItemStyle-Width="90px" ItemStyle-Wrap="false" ItemStyle-BorderWidth="1" ItemStyle-BackColor="ButtonFace" >
                            <ItemTemplate>
                            <a href="UserDetails.aspx?userid=<%#Eval("id")%>">
                                Update User
                            </a>
                            </ItemTemplate>
                        </asp:TemplateField> 
                    </Columns>
                    <HeaderStyle BackColor="#0063A6" ForeColor="White" />
                </asp:GridView>   

            </asp:View>
            <asp:View ID="View2" runat="server">

            <table>
            <tr>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td><asp:Label ID="Label1" runat="server" CssClass="lbl" Text="Lookup List Name" Font-Bold="true"></asp:Label></td>
                <td><asp:DropDownList ID="ddlListLookupName" runat="server" AutoPostBack="true" onselectedindexchanged="ddlListLookupName_SelectedIndexChanged" Font-Size="14px" ></asp:DropDownList></td>
            </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <asp:button ID="btnNewLookup" Text="Add New Lookup Value" runat="server" PostBackUrl="frmLookupDetails.aspx?lookuplistvalueid=0&lookuplistnameid=0" />
                    </td>
                    <td>
                    </td>
                    <td>
                            <asp:button ID="btnRefresh" Text="Refresh" runat="server" UseSubmitBehavior="true" />
                    <td>
                </tr>
            </table>
                <asp:GridView ID="gvLookup" runat="server" AutoGenerateColumns="false" ShowFooter="false" OnRowDataBound="gvJob_OnRowDataBound">
                    <Columns> 
                        <asp:TemplateField ItemStyle-BorderWidth="1" HeaderText="ID">
                        <ItemTemplate>
                            <a href="LookupDetails.aspx?lookuplistvalueid=<%#Eval("id")%>&lookuplistnameid=<%#Eval("LookupListName_ID")%>"><%# Eval("id") %></a>
                        </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:BoundField Visible="false" ItemStyle-BorderWidth="1" DataField="ID" HeaderText="ID" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="LookupListName_ID" Visible="false" HeaderText="LookupListName_ID" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="LookupListCondition" HeaderText="Condition" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="LookupListInt" HeaderText="Integer" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="LookupListCode" HeaderText="Code" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="LookupListDesc" HeaderText="Description" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="LookupListSortOrder" HeaderText="Sort Order" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="IsActive" HeaderText="Active" />
                        <asp:TemplateField ItemStyle-Width="90px" ItemStyle-Wrap="false" ItemStyle-BorderWidth="1" ItemStyle-BackColor="ButtonFace" >
                            <ItemTemplate>
                            <a href="LookupDetails.aspx?lookuplistvalueid=<%#Eval("id")%>&lookuplistnameid=<%#Eval("LookupListName_ID")%>">
                                Update Lookup
                            </a>
                            </ItemTemplate>
                        </asp:TemplateField> 
                    </Columns>
                    <HeaderStyle BackColor="#0063A6" ForeColor="White" />
                </asp:GridView>               

            </asp:View>

            <asp:View ID="View3" runat="server">

            <table>
            <tr>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td><asp:Label ID="Label2" runat="server" CssClass="lbl" Text="User Name" Font-Bold="true"></asp:Label></td>
                <td><asp:DropDownList ID="ddlLeaveUserName" runat="server" AutoPostBack="true" onselectedindexchanged="ddlLeaveUserName_SelectedIndexChanged" Font-Size="14px" ></asp:DropDownList></td>
            </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <asp:button ID="Button1" Text="Add New Leave/Training" runat="server" PostBackUrl="~/Admin/LeaveDetails.aspx?leaveid=0" />
                    </td>
                    <td>
                    </td>
                    <td>
                            <asp:button ID="Button2" Text="Refresh" runat="server" UseSubmitBehavior="true" />
                    <td>
                </tr>
            </table>
                <asp:GridView ID="gvLeave" runat="server" AutoGenerateColumns="false" ShowFooter="false" OnRowDataBound="gvLeave_OnRowDataBound">
                    <Columns> 
                        <asp:TemplateField ItemStyle-BorderWidth="1" HeaderText="ID">
                        <ItemTemplate>
                            <a href="LeaveDetails.aspx?leaveid=<%#Eval("ID")%>"><%# Eval("ID") %></a>
                        </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="ID" Visible="false" HeaderText="ID" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="UserDisplayName" HeaderText="User Name" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="LeaveType" HeaderText="Type" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="StartDate" HeaderText="Start Date" />
                        <asp:BoundField ItemStyle-BorderWidth="1" DataField="EndDate" HeaderText="End Date" />
                        <asp:TemplateField ItemStyle-Width="90px" ItemStyle-Wrap="false" ItemStyle-BorderWidth="1" ItemStyle-BackColor="ButtonFace" >
                            <ItemTemplate>
                            <a href="LeaveDetails.aspx?leaveid=<%#Eval("ID")%>">
                                Update Leave
                            </a>
                            </ItemTemplate>
                        </asp:TemplateField> 
                    </Columns>
                    <HeaderStyle BackColor="#0063A6" ForeColor="White" />
                </asp:GridView>               

            </asp:View>
 
          </asp:MultiView>
        </td>
      </tr>
    </table>
    </div>
  </asp:Content>
