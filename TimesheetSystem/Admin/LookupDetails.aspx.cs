﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TimesheetSystem.Admin
{
    public partial class LookupDetails : System.Web.UI.Page
    {
        int intIsActiveIndex;
        int[] intIsActiveID = new int[1000];
        String[] strIsActiveValue = new String[1000];

        int intLookupListNameIndex;
        int[] intLookupListNameID = new int[1000];
        String[] strLookupListNameValue = new String[1000];

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                setPermission();
                getLookup();
                setValues();
            }
            else
            {
                getLookup();
            }
        }

        public void setPermission()
        {
            //-- Set Permissions --//

            TimesheetSystem.MMWUser mmwUser = new MMWUser();
            mmwUser.getMMWUserDetails(HttpContext.Current.User.Identity.Name);

            if (mmwUser.strAccessRole != "Admin")
            {
                this.ddlLookupListName_ID.Enabled = false;
                this.ddlLookupListName_ID.BackColor = System.Drawing.Color.LightGray;
                this.txtLookupListInt.Enabled = false;
                this.txtLookupListInt.BackColor = System.Drawing.Color.LightGray;
                this.txtLookupListCode.Enabled = false;
                this.txtLookupListCode.BackColor = System.Drawing.Color.LightGray;
                this.txtLookupListDesc.Enabled = false;
                this.txtLookupListDesc.BackColor = System.Drawing.Color.LightGray;
                this.txtLookupListSortOrder.Enabled = false;
                this.txtLookupListSortOrder.BackColor = System.Drawing.Color.LightGray;
                this.ddlIsActive.Enabled = false;
                this.ddlIsActive.BackColor = System.Drawing.Color.LightGray;

                this.Save.Visible = false;
            }
        }

        public void getLookup()
        {
            //-- Is Active --//

            this.intIsActiveIndex = 0;
            this.intIsActiveID[this.intIsActiveIndex] = 0;
            this.strIsActiveValue[this.intIsActiveIndex] = "False";
            this.intIsActiveIndex++;
            this.intIsActiveID[this.intIsActiveIndex] = 1;
            this.strIsActiveValue[this.intIsActiveIndex] = "True";
            this.intIsActiveIndex++;

            //-- Lookup List Name Role --//

            this.intLookupListNameIndex = 0;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            {
                using (SqlCommand cmd = new SqlCommand("SELECT LookupListName_ID, LookupListName FROM uv_LookupListValue_Select GROUP BY LookupListName_ID, LookupListName ORDER BY LookupListName", conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        this.intLookupListNameID[this.intLookupListNameIndex] = (int)reader["LookupListName_ID"];
                        this.strLookupListNameValue[this.intLookupListNameIndex] = reader["LookupListName"].ToString();
                        this.intLookupListNameIndex++;
                    }

                    conn.Close();
                }
            }
        }

        public void setValues()
        {
            String strLookupListValueID = Request.QueryString["lookuplistvalueid"].ToString();
            String strLookupListNameID = Request.QueryString["lookuplistnameid"].ToString();

            if (strLookupListValueID == "")
            {
                strLookupListValueID = "0";
            }

            //-- Edit Lookup --//

            if (strLookupListValueID != "0")
            {
                this.FormHeading.Text = "Edit Lookup";

                //-- Load User Data --//

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                {
                    String strCommand = "";
                    strCommand = "SELECT ID, LookupListName_ID, LookupListCondition, LookupListInt, LookupListCode, LookupListDesc, LookupListSortOrder, IsActive FROM LookupListValue WITH (NOLOCK) WHERE ID = " + strLookupListValueID;

                    using (SqlCommand cmd = new SqlCommand(strCommand, conn))
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            this.txtID.Text = strLookupListValueID;
                            this.txtLookupListCondition.Text = reader["LookupListCondition"].ToString();
                            this.txtLookupListInt.Text = reader["LookupListInt"].ToString();
                            this.txtLookupListCode.Text = reader["LookupListCode"].ToString();
                            this.txtLookupListDesc.Text = reader["LookupListDesc"].ToString();
                            this.txtLookupListSortOrder.Text = reader["LookupListSortOrder"].ToString();

                            for (int a = 0; a < this.intLookupListNameIndex; a++)
                            {
                                this.ddlLookupListName_ID.Items.Insert(a, this.strLookupListNameValue[a]);
                                if (this.intLookupListNameID[a] == (int)reader["LookupListName_ID"])
                                {
                                    this.ddlLookupListName_ID.SelectedIndex = a;
                                }
                            }
                            if (this.ddlLookupListName_ID.SelectedIndex < 0 && this.intLookupListNameIndex > 0)
                            {
                                this.ddlLookupListName_ID.SelectedIndex = 0;
                            }

                            for (int a = 0; a < this.intIsActiveIndex; a++)
                            {
                                this.ddlIsActive.Items.Insert(a, this.strIsActiveValue[a]);
                                if (this.intIsActiveID[a] == int.Parse(reader["IsActive"].ToString()))
                                {
                                    this.ddlIsActive.SelectedIndex = a;
                                }
                            }
                            if (this.ddlIsActive.SelectedIndex < 0 && this.intIsActiveIndex > 0)
                            {
                                this.ddlIsActive.SelectedIndex = 1;
                            }

                            //--//
                        }

                        reader.Close();
                        conn.Close();
                    }
                }
            }
            else
            {
                this.FormHeading.Text = "New Lookup Value";
                int intLookupListNameID = int.Parse(strLookupListNameID);

                this.txtID.Text = strLookupListValueID;
                this.txtLookupListCondition.Text = "";
                this.txtLookupListInt.Text = "";
                this.txtLookupListCode.Text = "";
                this.txtLookupListDesc.Text = "";
                this.txtLookupListSortOrder.Text = "1";

                for (int a = 0; a < this.intLookupListNameIndex; a++)
                {
                    this.ddlLookupListName_ID.Items.Insert(a, this.strLookupListNameValue[a]);
                    if (this.intLookupListNameID[a] == intLookupListNameID)
                    {
                        this.ddlLookupListName_ID.SelectedIndex = a;
                    }
                }
                if (this.ddlLookupListName_ID.SelectedIndex < 0 && this.intLookupListNameIndex > 0)
                {
                    this.ddlLookupListName_ID.SelectedIndex = 0;
                }

                for (int a = 0; a < this.intIsActiveIndex; a++)
                {
                    this.ddlIsActive.Items.Insert(a, this.strIsActiveValue[a]);
                }
                if (this.intIsActiveIndex > 0)
                {
                    this.ddlIsActive.SelectedIndex = 1;
                }
            }
        }

        public void saveDetails()
        {
            String strLookupListValueID = Request.QueryString["lookuplistvalueid"].ToString();
            if (strLookupListValueID == "")
            {
                strLookupListValueID = "0";
            }

            String strLookupListCondition = this.txtLookupListCondition.Text;
            String strLookupListInt = this.txtLookupListInt.Text;
            String strLookupListCode = this.txtLookupListCode.Text;
            String strLookupListDesc = this.txtLookupListDesc.Text;
            int intLookupListSortOrder = int.Parse(this.txtLookupListSortOrder.Text);

            int intLookupListName_ID = 0;
            if (this.ddlLookupListName_ID.SelectedIndex > -1)
            {
                intLookupListName_ID = this.intLookupListNameID[this.ddlLookupListName_ID.SelectedIndex];
            }

            int intIsActive = 0;
            if (this.ddlIsActive.SelectedIndex == 1)
            {
                intIsActive = 1;
            }
            if (strLookupListValueID != "0")
            {
                int intLookupListValueID = int.Parse(strLookupListValueID);

                //-- Update User --//

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

                SqlCommand cmd = new SqlCommand("UPDATE LookupListValue SET LookupListName_ID = @LookupListName_ID, LookupListCondition = @LookupListCondition, LookupListInt = @LookupListInt, LookupListCode = @LookupListCode, LookupListDesc = @LookupListDesc, LookupListSortOrder = @LookupListSortOrder, IsActive = @IsActive, DateUpdated=GETDATE() WHERE ID = @ID", conn);
                cmd.Parameters.Add("@LookupListName_ID", SqlDbType.Int).Value = intLookupListName_ID;
                if (strLookupListCondition == "")
                {
                    cmd.Parameters.Add("@LookupListCondition", SqlDbType.NVarChar).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LookupListCondition", SqlDbType.NVarChar).Value = strLookupListCondition;
                }
                if (strLookupListInt == "")
                {
                    cmd.Parameters.Add("@LookupListInt", SqlDbType.Int).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LookupListInt", SqlDbType.Int).Value = int.Parse(strLookupListInt);
                }
                cmd.Parameters.Add("@LookupListCode", SqlDbType.NChar).Value = strLookupListCode;
                cmd.Parameters.Add("@LookupListDesc", SqlDbType.NChar).Value = strLookupListDesc;
                cmd.Parameters.Add("@LookupListSortOrder", SqlDbType.Int).Value = intLookupListSortOrder;
                cmd.Parameters.Add("@IsActive", SqlDbType.Int).Value = intIsActive;
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = intLookupListValueID;

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            else
            {
                //-- Insert New User --//

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

                SqlCommand cmd = new SqlCommand("INSERT INTO LookupListValue (LookupListName_ID, LookupListCondition, LookupListInt, LookupListCode, LookupListDesc, LookupListSortOrder, IsActive, DateInserted, DateUpdated) SELECT @LookupListName_ID, @LookupListCondition, @LookupListInt, @LookupListCode, @LookupListDesc, @LookupListSortOrder, @IsActive, GETDATE(), GETDATE()", conn);
                cmd.Parameters.Add("@LookupListName_ID", SqlDbType.Int).Value = intLookupListName_ID;
                cmd.Parameters.Add("@LookupListCondition", SqlDbType.NVarChar).Value = strLookupListCondition;
                if (strLookupListInt == "")
                {
                    cmd.Parameters.Add("@LookupListInt", SqlDbType.Int).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LookupListInt", SqlDbType.Int).Value = int.Parse(strLookupListInt);
                }
                cmd.Parameters.Add("@LookupListCode", SqlDbType.NChar).Value = strLookupListCode;
                cmd.Parameters.Add("@LookupListDesc", SqlDbType.NChar).Value = strLookupListDesc;
                cmd.Parameters.Add("@LookupListSortOrder", SqlDbType.Int).Value = intLookupListSortOrder;
                cmd.Parameters.Add("@IsActive", SqlDbType.Int).Value = intIsActive;

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            saveDetails();
            Response.Redirect("~/Admin/Admin.aspx");
        }
    }
}