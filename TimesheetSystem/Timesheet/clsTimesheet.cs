﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TimesheetSystem
{
    public class clsTimesheet
    {
        public int Timesheet_ID;
        public string JobNo;
        public int RunNo;
        public int JobRun_ID;
        public string JobType;
        public string Client;
        public int SourceID;
        public string SourceCode;
        public string CampaignID;
        public string CampaignName;
        public string TaskID;
        public string JobDescription;
        public int Programmer_ID;
        public int ClientService_ID;
        public int AccountManager_ID;
        public DateTime BilledOn;
        public string Type;
        public DateTime StartTime;
        public DateTime EndTime;
        public decimal TimeSpent;
        public string OperationCode;
        public string Comments;
        public string ReasonExtraWork;
        public byte Chargeable;
        public byte Billed;
        public byte Automated;
        public int Filecount;
        public int InputRecords;
        public int OutputRecords;
        public int ExcludedRecords;
        public int IsProcessed;
        public DateTime DateInserted;
        public DateTime DateUpdated;
        public string NonChargeableReason;

        public string SaveError;

        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

        // Date to String Conversion --//

        public string strDate;
        public string strHH;
        public string strMM;

        public clsTimesheet()
        {
            //-- Initalise Defaults --//

            //-- TO DO -- Standardise Source Code - TechOps vs Tech Ops

            this.Timesheet_ID = 0;
            this.JobNo = "";
            this.RunNo = 0;
            this.JobRun_ID = 0;
            this.JobType = "";
            this.SourceID = 0;
            this.SourceCode = "TechOps";
            this.CampaignID = "";
            this.CampaignName = "";
            this.TaskID = "";
            this.JobDescription = "";
            this.Programmer_ID = 0;
            this.ClientService_ID = 0;
            this.AccountManager_ID = 0;
            this.BilledOn = DateTime.MinValue;
            this.Type = "";
            this.StartTime = DateTime.MinValue;
            this.EndTime = DateTime.MinValue;
            this.TimeSpent = 0;
            this.OperationCode = "";
            this.Comments = "";
            this.ReasonExtraWork = "";
            this.Chargeable = 1;
            this.Billed = 0;
            this.Automated = 0;
            this.Filecount = 0;
            this.InputRecords = 0;
            this.OutputRecords = 0;
            this.ExcludedRecords = 0;
            this.IsProcessed = 0;
            this.DateInserted = DateTime.MinValue;
            this.DateUpdated = DateTime.MinValue;
            this.NonChargeableReason = "";

            this.SaveError = "";
        }

        public bool readTimesheet()
        {
            if (this.Timesheet_ID == 0)
            {
                return false;
            }

            //-- Load Task Details --//

            string strCommand = "SELECT JobNo, RunNo,JobRun_ID,JobType,Client,SourceID,SourceCode,CampaignID,CampaignName,TaskID,JobDescription,Programmer_ID,ClientService_ID,AccountManager_ID,BilledOn,Type,StartTime,EndTime,TimeSpent,OperationCode,Comments,ReasonExtraWork,Chargeable,Billed,Automated,Filecount,InputRecords,OutputRecords,ExcludedRecords,IsProcessed,DateInserted,DateUpdated,NonChargeableReason FROM uv_Timesheet WHERE ID = " + this.Timesheet_ID.ToString(); ;

            using (SqlCommand cmd = new SqlCommand(strCommand, conn))
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    this.JobNo = reader["JobNo"].ToString();
                    if (reader["RunNo"] != DBNull.Value) this.RunNo = (int)reader["RunNo"];
                    if (reader["JobRun_ID"] != DBNull.Value) this.JobRun_ID = (int)reader["JobRun_ID"];
                    this.JobType = reader["JobType"].ToString();
                    this.Client = reader["Client"].ToString();
                    if (reader["SourceID"] != DBNull.Value) this.SourceID = (int)reader["SourceID"];
                    this.SourceCode = reader["SourceCode"].ToString();
                    this.CampaignID = reader["CampaignID"].ToString();
                    this.CampaignName = reader["CampaignName"].ToString();
                    this.TaskID = reader["TaskID"].ToString();
                    this.JobDescription = reader["JobDescription"].ToString();
                    if (reader["Programmer_ID"] != DBNull.Value) this.Programmer_ID = (int)reader["Programmer_ID"];
                    if (reader["ClientService_ID"] != DBNull.Value) this.ClientService_ID = (int)reader["ClientService_ID"];
                    if (reader["AccountManager_ID"] != DBNull.Value) this.AccountManager_ID = (int)reader["AccountManager_ID"];
                    //this.BilledOn = (DateTime)reader["BilledOn"];
                    this.Type = reader["Type"].ToString();
                    this.StartTime = (DateTime)reader["StartTime"];
                    //this.EndTime = (DateTime)reader["EndTime"];
                    if (reader["EndTime"] != DBNull.Value) this.EndTime = (DateTime)reader["EndTime"];
                    if (reader["TimeSpent"] != DBNull.Value) this.TimeSpent = (decimal)reader["TimeSpent"];
                    this.OperationCode = reader["OperationCode"].ToString();
                    this.Comments = reader["Comments"].ToString();
                    this.ReasonExtraWork = reader["ReasonExtraWork"].ToString();
                    if (reader["Chargeable"] != DBNull.Value) this.Chargeable = (byte)reader["Chargeable"];
                    if (reader["Billed"] != DBNull.Value) this.Billed = (byte)reader["Billed"];
                    if (reader["Automated"] != DBNull.Value) this.Automated = (byte)reader["Automated"];
                    if (reader["Filecount"] != DBNull.Value) this.Filecount = (int)reader["Filecount"];
                    if (reader["InputRecords"] != DBNull.Value) this.InputRecords = (int)reader["InputRecords"];
                    if (reader["OutputRecords"] != DBNull.Value) this.OutputRecords = (int)reader["OutputRecords"];
                    if (reader["ExcludedRecords"] != DBNull.Value) this.ExcludedRecords = (int)reader["ExcludedRecords"];
                    //if (reader["IsProcessed"] != DBNull.Value) this.IsProcessed = (int)reader["IsProcessed"];
                    if (reader["DateInserted"] != DBNull.Value) this.DateInserted = (DateTime)reader["DateInserted"];
                    if (reader["DateUpdated"] != DBNull.Value) this.DateUpdated = (DateTime)reader["DateUpdated"];
                    if (reader["NonChargeableReason"] != DBNull.Value) this.NonChargeableReason = reader["NonChargeableReason"].ToString();
                }

                reader.Close();
                conn.Close();
            }

            return true;
        }

        public bool saveTimesheet(string startTime)
        {
            this.SaveError = "";

            string strCmd = "";
            if (this.Billed == 1)
            {
                strCmd += "UPDATE Timesheet SET Billed=@Billed, BilledOn=GETDATE()";
            }
            else
            {
                strCmd += "UPDATE Timesheet SET Billed=@Billed ";
            }

            if (this.RunNo == -1)
                strCmd += " WHERE JobNo=@JobNo  AND RunNo IS NULL  AND"; 
            else
                strCmd += " WHERE JobNo=@JobNo  AND RunNo=@RunNo  AND";

            if (this.OperationCode != "")
                strCmd += " LineItem=@OperationCode AND"; 
            else
                strCmd += " LineItem IS NULL AND";

            if (this.CampaignID != "")
                strCmd += " CampaignID=@CampaignID AND";
            else
                strCmd += " CampaignID IS NULL AND";

            strCmd += " SourceCode=@SourceCode  AND";

            if (this.Client != "")
                strCmd += " Client=@Client AND";
            else
                strCmd += " Client IS NULL AND";

            strCmd += " Convert(date, StartTime) = @StartTime ";

            SqlCommand cmd = new SqlCommand(strCmd, conn);
            cmd.Parameters.Add("@JobNo", SqlDbType.Int).Value = int.Parse(this.JobNo);

            if (this.RunNo != -1)
                cmd.Parameters.Add("@RunNo", SqlDbType.Int).Value = this.RunNo;
            else
                cmd.Parameters.Add("@RunNo", SqlDbType.Int).Value = DBNull.Value;

            if (this.OperationCode == "")
                cmd.Parameters.Add("@OperationCode", SqlDbType.VarChar).Value = DBNull.Value; 
            else
                cmd.Parameters.Add("@OperationCode", SqlDbType.NVarChar).Value = this.OperationCode;

            cmd.Parameters.Add("@CampaignID", SqlDbType.VarChar).Value = this.CampaignID;
            cmd.Parameters.Add("@SourceCode", SqlDbType.VarChar).Value = this.SourceCode;
            cmd.Parameters.Add("@Billed", SqlDbType.TinyInt).Value = this.Billed;
            cmd.Parameters.Add("@Client", SqlDbType.VarChar).Value = this.Client;
            cmd.Parameters.Add("@StartTime", SqlDbType.VarChar).Value = startTime;

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            
            return true;
        }
    }
}