﻿<%@ Page Title="User Detail" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserDetails.aspx.cs" Inherits="TimesheetSystem.Admin.UserDetails" %>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
<script type="text/javascript">
    function Validate() {

        var strUserName = document.getElementById("txtUserName");
        var strUserDisplayName = document.getElementById("txtUserDisplayName");

        var strValidation = "";
        if (strUserName.value == "") {
            strValidation = strValidation + "\n  - User Name.";
        }
        if (strUserDisplayName.value == "") {
            strValidation = strValidation + "\n  - User Display Name.";
        }

        if (strValidation != "") {

            alert("Please enter:" + strValidation);

            return false;

        } else {

            return true;
        }


        /*
        var strEstimate = document.getElementById("txtJobEstimate");
        var txtJobStage = document.getElementById("ddlJobStage");
        var txtJobStatus = document.getElementById("ddlJobStatus");
        var txtOriginalJobStatus = document.getElementById("txtOriginalJobStatus");
        var txtAuditDone = document.getElementById("txtAuditDone");

        var strValidation = "";
        if (txtOriginalJobStatus.value != "" && txtOriginalJobStatus.value != "Not Started" && parseInt(strEstimate.value) == 0) {
            strValidation = strValidation + "\nYou have not entered an estimated time.";
        }
        if ((txtJobStage.value == "LSO Data" || txtJobStage.value == "Final Data" || txtJobStage.value == "LSO Laser") && txtJobStatus.value == "Done" && txtAuditDone.value == "0") {
            strValidation = strValidation + "\nYou have not completed an Audit.";
        }

        if (strValidation != "") {

            if (confirm(strValidation + "\n Are you sure you want to save?")) {
                return true;
            } else {
                return false;
            }
        }
        else {

            this.txtAuditDone


            return true;

        }
        */
    }
</script> 
<div>
<table>
    <tr>
        <td></td>
        <td></td>
        <td><asp:Label ID="FormHeading" runat="server" Font-Size="Large" Font-Bold="true" BackColor="White">Edit User</asp:Label></td>
    </tr>
    <tr>
        <td></td>
        <td><asp:Label ID="Label8" runat="server" CssClass="lbl" Text="ID" Font-Bold="true"></asp:Label></td>
        <td><asp:TextBox ID="txtID" runat="server" Font-Size="14px" ReadOnly="true" BackColor="ButtonFace"></asp:TextBox></td>
    </tr>
    <tr>
        <td></td>
        <td><asp:Label ID="Label1" runat="server" CssClass="lbl" Text="User Name" Font-Bold="true"></asp:Label></td>
        <td><asp:TextBox ID="txtUserName" ClientIDMode="Static" MaxLength="50" runat="server" Font-Size="14px"></asp:TextBox></td>
    </tr>
    <tr>
        <td></td>
        <td><asp:Label ID="Label2" runat="server" CssClass="lbl" Text="Display Name" Font-Bold="true"></asp:Label></td>
        <td><asp:TextBox ID="txtUserDisplayName" ClientIDMode="Static" MaxLength="50" runat="server" Font-Size="14px" ></asp:TextBox></td>
    </tr>
    <tr>
        <td></td>
        <td><asp:Label ID="Label17" runat="server" CssClass="lbl" Text="First Name" Font-Bold="true"></asp:Label></td>
        <td><asp:TextBox ID="txtFirstName" MaxLength="50" runat="server" Font-Size="14px" ></asp:TextBox></td>
    </tr>
    <tr>
        <td></td>
        <td><asp:Label ID="Label21" runat="server" CssClass="lbl" Text="Last Name" Font-Bold="true"></asp:Label></td>
        <td><asp:TextBox ID="txtLastName" MaxLength="50" runat="server" Font-Size="14px" ></asp:TextBox></td>
    </tr>
    <tr>
        <td></td>
        <td><asp:Label ID="Label3" runat="server" CssClass="lbl" Text="Access Role" Font-Bold="true"></asp:Label></td>
        <td><asp:DropDownList ID="ddlAccessRole" runat="server" Font-Size="14px" ></asp:DropDownList></td>
    </tr>
    <tr>
        <td></td>
        <td><asp:Label ID="Label4" runat="server" CssClass="lbl" Text="Active" Font-Bold="true"></asp:Label></td>
        <td><asp:DropDownList ID="ddlIsActive" runat="server" Font-Size="14px" ></asp:DropDownList></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td style="text-align:center">
        <asp:Button ID="Save" runat="server" UseSubmitBehavior="true" OnClientClick="return Validate();" OnClick="Save_Click" Text="Save"></asp:Button>
        <asp:Button ID="Cancel" runat="server" CausesValidation="false" PostBackUrl="~/Admin/Admin.aspx" Text="Cancel"></asp:Button>
        </td>
    </tr>
    </table>    
    </div>
</asp:Content>

