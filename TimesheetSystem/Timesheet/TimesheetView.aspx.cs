﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;


namespace TimesheetSystem
{
    public partial class TimesheetView : System.Web.UI.Page
    {
        String strAccessRole = "";
        String strUserDisplay = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    setPermission();
                    loadDetails1();           
                    BindGrid();
                }
                else
                {
                    Response.Redirect("/Account/Login.aspx");
                }
            }
            else
            {
                //Session["txtJobNumber"] = this.txtJobNumber.Text;
                //Session["txtRunNumber"] = this.txtRunNumber.Text;
                //Session["txtClient"] = this.txtClient.Text;
                //Session["ddlSource"] = this.ddlSource.SelectedValue;
                //Session["ddlBilled"] = this.ddlBilled.SelectedValue;
            }
        }

        public Boolean setPermission()
        {
            //-- Set Permissions --//

            MMWUser mmwUser = new MMWUser();
            mmwUser.getMMWUserDetails(HttpContext.Current.User.Identity.Name);

            if (mmwUser.strAccessRole != "Finance")
            {
                this.btnSelectAll.Enabled = false;
                this.btnSelectAll.BorderColor = System.Drawing.Color.LightGray;
                this.btnSelectAll.BackColor = System.Drawing.Color.LightGray;

                this.btnUnSelectAll.Enabled = false;
                this.btnUnSelectAll.BorderColor = System.Drawing.Color.LightGray;
                this.btnUnSelectAll.BackColor = System.Drawing.Color.LightGray;

                this.btnClear.Enabled = false;
                this.btnClear.BorderColor = System.Drawing.Color.LightGray;
                this.btnClear.BackColor = System.Drawing.Color.LightGray;

                this.btnSave.Enabled = false;
                this.btnSave.BorderColor = System.Drawing.Color.LightGray;
                this.btnSave.BackColor = System.Drawing.Color.LightGray;

                return false;
            }

            return true;
        }

        public void loadDetails1()
        {
            //-- Get User Details --//

            MMWUser mmwUser = new MMWUser();
            mmwUser.getMMWUserDetails(HttpContext.Current.User.Identity.Name);

            strAccessRole = mmwUser.strAccessRole;
            strUserDisplay = mmwUser.strUserDisplayName;

            this.ddlBilled.Items.Clear();
            this.ddlBilled.Items.Add("(ALL)");
            this.ddlBilled.Items.Add("No");
            this.ddlBilled.Items.Add("Yes");
            this.ddlSource.SelectedIndex = 0;

            this.ddlSource.Items.Clear();
            this.ddlSource.Items.Add("(ALL)");
            this.ddlSource.Items.Add("TIMESHEET");
            this.ddlSource.Items.Add("JIRA");      
            this.ddlSource.Items.Add("ORCA");      
            this.ddlSource.SelectedIndex = 0;
     
            if (Session["ddlSource"] != null)
            {
                this.ddlSource.SelectedValue = (string)Session["ddlSource"];
            }

            if (Session["ddlBilled"] != null)
            {
                this.ddlBilled.SelectedValue = (string)Session["ddlBilled"];
            }
        }
        private void BindGrid()
        {
            String strJobNumber = this.txtJobNumber.Text;
            String strRunNumber = this.txtRunNumber.Text;
            String strClient = this.txtClient.Text;
            String strStart = this.txtStartDate.Text;
            String strEnd = this.txtEndDate.Text;

            String dateFilterCmd = "";
            if (strStart == "")
            {
                // If From date is not set, we don't want all the the records to be loaded
                strEnd = "";
            }

            dateFilterCmd = " WHERE convert(date,StartTime) BETWEEN '" + strStart + "' AND '" + strEnd + "'";

            String jobFilterCmd = "";
            if (strJobNumber != "")
            {
                jobFilterCmd += " AND ";
                jobFilterCmd += "JobNo = ";
                jobFilterCmd += "'";
                jobFilterCmd += strJobNumber;
                jobFilterCmd += "'";
            }
            if (strRunNumber != "")
            {
                jobFilterCmd += " AND ";
                jobFilterCmd += "RunNo = ";
                jobFilterCmd += strRunNumber;
            }
            if (strClient != "")
            {
                jobFilterCmd += " AND ";
                jobFilterCmd += "Client LIKE '%";
                jobFilterCmd += strClient;
                jobFilterCmd += "%'";
            }

            if (this.ddlBilled.SelectedValue == "No")
            {
                jobFilterCmd += " AND Billed='No'";
            }
            else if (this.ddlBilled.SelectedValue == "Yes")
            {
                jobFilterCmd += " AND Billed='Yes'";
            }
            else
            {
                // (ALL) 
                jobFilterCmd += " AND Billed IN ('Yes', 'No') ";
            }

            string sourceCodeFilter = "";
            if (this.ddlSource.SelectedValue == "(ALL)")
            {
                sourceCodeFilter = " AND SourceCode IN ('ORCA_LP', 'ORCA_DP', 'JIRA', 'Timesheet')";
            }
            else if (this.ddlSource.SelectedValue == "TIMESHEET")
            {
                sourceCodeFilter = " AND SourceCode = 'Timesheet'";
            }
            else if (this.ddlSource.SelectedValue == "JIRA")
            {
                sourceCodeFilter = " AND SourceCode = 'JIRA'";
            }
            else
            {
                sourceCodeFilter = " AND SourceCode IN ('ORCA_LP', 'ORCA_DP')";
            }

            jobFilterCmd += "AND Chargeable='Yes'";
            jobFilterCmd += sourceCodeFilter;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                                
            //-- Populate Grid --//

            string cmdstr = "";
            cmdstr =  " SELECT JobNo,RunNo,Convert(date, StartTime) AS StartTime,Client,SourceCode,CampaignID,LineItem,Chargeable,Billed,BilledOn,MAX(DateInserted) AS DateInserted,MAX(DateUpdated) AS DateUpdated,SUM(ISNULL(TimeSpent,0)) AS TimeSpent ";     
            cmdstr += " FROM uv_Timesheet WITH (NOLOCK) " + dateFilterCmd + jobFilterCmd;
            cmdstr += " GROUP BY JobNo, RunNo, Convert(date, StartTime), Client, SourceCode, CampaignID, LineItem, Chargeable, Billed, BilledOn ";
            cmdstr += " ORDER BY JobNo, RunNo, StartTime ";
            
            using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(cmdstr))
                {
                    using (SqlDataAdapter adp = new SqlDataAdapter())
                    {
                        cmd.Connection = conn;
                        adp.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            

                            adp.Fill(dt);
                            this.gvTimesheet.DataSource = dt;
                            this.gvTimesheet.DataBind();

                            if (dt.Rows.Count > 0)
                            {
                                decimal total = dt.AsEnumerable().Sum(row => row.Field<decimal>("TimeSpent"));                           

                                this.gvTimesheet.FooterRow.Cells[1].Text = "Total";
                                this.gvTimesheet.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                                this.gvTimesheet.FooterRow.Cells[1].CssClass = "gridCellHighlight";

                                this.gvTimesheet.FooterRow.Cells[9].Text = total.ToString("N2");
                                this.gvTimesheet.FooterRow.Cells[9].CssClass = "gridCellHighlight";
                            }
                        }
                    }
                }
            }
        }

        protected void gvTimesheet_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String temp = e.Row.Cells[1].Text;
                String temp1 = e.Row.Cells[2].Text;
                String temp2 = e.Row.Cells[3].Text;
                String source = e.Row.Cells[4].Text;
                
                // DO NOT allow changes to the ORCA timesheet records
                // If user doesn't have permission, disabled the Billed? column checkboxes
                if (!setPermission())   // || source=="ORCA_LP" || source=="ORCA_DP" || source == "Timesheet")  
                {
                    CheckBox chkRow = (e.Row.Cells[5].FindControl("cbBilledSelect") as CheckBox);
                    if (source == "Timesheet" || source == "JIRA")
                        chkRow.Enabled = true;
                    else
                        chkRow.Enabled = false;
                }
            }
        }

        protected void SelectAll_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvTimesheet.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkRow = (row.FindControl("cbBilledSelect") as CheckBox);
                    // DO NOT allow changes to the ORCA timesheet records
                    if (row.Cells[4].Text == "ORCA_DP" || row.Cells[4].Text == "ORCA_LP")
                    {
                        //chkRow.Enabled = false;
                        continue;
                    }
                    chkRow.Checked = true;
                }
            }
        }

        protected void UnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvTimesheet.Rows)
            {
                CheckBox chkRow = (row.FindControl("cbBilledSelect") as CheckBox);
                if (row.Cells[4].Text == "ORCA_DP" || row.Cells[4].Text == "ORCA_LP")
                {
                    //chkRow.Enabled = false;
                    continue;
                }
                chkRow.Checked = false;                
            }        
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            Boolean isModified = false;

            foreach (GridViewRow row in gvTimesheet.Rows)
            {
                 Boolean isChanged = ((row.RowState & DataControlRowState.Edit) == DataControlRowState.Edit);

                if (row.RowType == DataControlRowType.DataRow)
                {
                    String jobNo = row.Cells[1].Text;
                    String runNo = row.Cells[2].Text;
                    String startTime = row.Cells[3].Text;
                    String source = row.Cells[4].Text;
                    String client = row.Cells[5].Text;;
                    String campaignID = row.Cells[6].Text;
                    String lineItem = row.Cells[7].Text;
                    String currentBilledFlag = row.Cells[12].Text;            
                  
                    CheckBox chkRow = (row.FindControl("cbBilledSelect") as CheckBox);

                    bool isBilledFlagChanged = ((currentBilledFlag == "Yes" && chkRow.Checked == false) || (currentBilledFlag == "No" && chkRow.Checked == true));
                    if (!isBilledFlagChanged || (source == "ORCA_DP" || source == "ORCA_LP"))
                    {
                        // Ignore timesheet records from ORCA
                        // Billed flag is not changed. Do nothing.. just go the next record..
                        continue;
                    }

                    isModified = true;

                    byte isBilled = 0;
                    if (chkRow.Checked)
                    {
                        isBilled = 1;
                    }

                    clsTimesheet timesheet = new clsTimesheet();
                    timesheet.JobNo = jobNo;
                    if (runNo == "&nbsp;") // Blank
                        timesheet.RunNo = -1;
                    else
                        timesheet.RunNo = int.Parse(runNo);

                    if (client == "&nbsp;") // Blank
                        timesheet.Client = "";
                    else
                        timesheet.Client = client;

                    if (campaignID == "&nbsp;") // Blank
                    {
                        timesheet.CampaignID = "";
                    }
                    else
                    {
                        timesheet.CampaignID = campaignID;
                    }

                    if (lineItem == "&nbsp;") // Blank
                        timesheet.OperationCode = "";
                    else
                        timesheet.OperationCode = lineItem;

                    timesheet.SourceCode = source;
                    timesheet.Billed = isBilled;
                   
                    timesheet.saveTimesheet(startTime);
                }
            }

            if (isModified)
            {
                // Reload the gridview with the updated data
                BindGrid();
            }
        }

        protected void Clear_Click(object sender, EventArgs e)
        {
            this.txtJobNumber.Text = "";
            this.txtRunNumber.Text = "";
            this.txtClient.Text = "";
            this.ddlBilled.SelectedIndex = 0;
            this.ddlSource.SelectedIndex = 0;

            BindGrid();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void ExportExcel_Click(object sender, EventArgs e)
        {
            Response.ClearContent();

            DateTime thisDay = DateTime.Today;
            String fName = "BilledJobsReport_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xls";
            String attachParam = "attachment; filename=" + fName; 
            Response.AddHeader("content-disposition", attachParam);  
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            this.gvTimesheet.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }

        protected void SetDate_Click(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void txtJobNumber_TextChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void txtRunNumber_TextChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void txtStartDate_TextChanged(object sender, EventArgs e)
        {
            //if (this.txtEndDate.Text != "")
                BindGrid();
        }

        protected void txtEndDate_TextChanged(object sender, EventArgs e)
        {
            //if (this.txtStartDate.Text != "")
                BindGrid();
        }

        protected void txtClient_TextChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void ddlBilled_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
            
        protected void cbBilledSelect_CheckedChanged(object sender, EventArgs e)
        {
        }

        protected void ddlSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}